CREATE TABLE employee (
	employee_id INT NOT NULL,
    first_name VARCHAR(30),
    last_name VARCHAR(30),
    email VARCHAR(30),
    phone_number VARCHAR(30),
    hire_date DATE,
    job_id VARCHAR(20),
    salary DECIMAL(7, 2),
    commision_pct DECIMAL(3, 2),
    manager_id INT,
    department_id INT,
    PRIMARY KEY(employee_id)
);


USE summativedb;

SHOW GLOBAL VARIABLES LIKE 'local_infile';
SET GLOBAL local_infile = true; 
SHOW VARIABLES LIKE 'secure_file_priv';

LOAD DATA LOCAL INFILE 'C:\Program Files\MySQL\MySQL Server 8.0\uploads\summative2.txt'
INTO TABLE employee
COLUMNS TERMINATED BY '|' 
LINES TERMINATED BY '\n'
IGNORE 3 LINES;