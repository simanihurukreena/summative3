CREATE TABLE departement (
	departement_id INT NOT NULL,
    departement_name VARCHAR(30),
    manager_id INT,
    location_id INT,
    PRIMARY KEY(departement_id)
);

USE summativedb;

SHOW GLOBAL VARIABLES LIKE 'local_infile';
SET GLOBAL local_infile = true; 
SHOW VARIABLES LIKE 'secure_file_priv';

LOAD DATA LOCAL INFILE 'C:\Program Files\MySQL\MySQL Server 8.0\uploads\summative2-1.txt'
INTO TABLE departement
COLUMNS TERMINATED BY '|' 
LINES TERMINATED BY '\n'
IGNORE 3 LINES;