SELECT st.name AS Nama, ls.name AS Pelajaran, scr.score AS Nilai
FROM summativedb.score scr
LEFT JOIN summativedb.student st ON st.id = scr.student_id
LEFT JOIN summativedb.lesson ls ON ls.id = scr.lesson_id;