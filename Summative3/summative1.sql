CREATE DATABASE summativedb;

USE summativedb;

CREATE TABLE Student (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    surname VARCHAR(50),
    birthdate DATE,
    gender VARCHAR(15),
    PRIMARY KEY(id)
);

CREATE TABLE Lesson (
	id INT NOT NULL AUTO_INCREMENT,
    name VARCHAR(50),
    level INT,
    PRIMARY KEY(id)
);

CREATE TABLE Score (
	id INT NOT NULL AUTO_INCREMENT,
    student_id INT NOT NULL,
    lesson_id INT NOT NULL,
    score INT,
    PRIMARY KEY(id),
    FOREIGN KEY(student_id) REFERENCES Student(id),
    FOREIGN KEY(lesson_id) REFERENCES Lesson(id)
);

INSERT INTO Student(name, surname, birthdate, gender) VALUES
('Maharani Kanya', 'Kanya', '1999-04-02', 'Female'),
('Buffen Sung', 'Buffen', '1998-08-20', 'Male'),
('Novenia Diani', 'Nia', '2000-04-04', 'Female'),
('Reenasi', 'Ena', '1999-08-06', 'Female'),
('Aquilae Beate', 'Aqui', '1999-01-22', 'Male');

INSERT INTO Lesson(name, level) VALUES 
('Math', 1), ('English', 1), ('Physic', 1), 
('Biologycal', 1), ('Sport', 1);

INSERT INTO Score(student_id, lesson_id, score) VALUES
(1, 1, 92), (1, 2, 91), (1, 3, 85), (1, 4, 95), (1, 5, 85),
(2, 1, 90), (2, 2, 80), (2, 3, 85), (2, 4, 100), (2, 5, 70),
(3, 1, 75), (3, 2, 90), (3, 3, 100), (3, 4, 85), (3, 5, 80),
(4, 1, 100), (4, 2, 90), (4, 3, 95), (4, 4, 98), (4, 5, 90),
(5, 1, 100), (5, 2, 90), (5, 3, 80), (5, 4, 70), (5, 5, 92);

SELECT * FROM summativedb.Student;
SELECT * FROM summativedb.Lesson;
SELECT * FROM summativedb.Score;
