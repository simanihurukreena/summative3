SELECT departement_id AS ID, departement_name AS Nama, YEAR(hire_date) AS Tahun, 
		COUNT(employee_id) AS 'Jumlah Karyawan'
FROM departement de
LEFT JOIN employee em ON em.department_id = de.departement_id
GROUP BY de.departement_id;